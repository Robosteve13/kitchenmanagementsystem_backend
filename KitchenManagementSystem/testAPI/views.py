from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.db.models import F
from django.forms.models import model_to_dict
import json
import pymongo
from .models import Users, Foods, Recipes, Ingredients, Pantry
import uuid
import datetime

from pyquery import PyQuery as pq
from lxml import etree
import re

# createUser
# getToken
# refreshToken
# createItem
# getItem
# deleteItem
# getRecipes
# getRecipe
# getSuggestions
# dismissSuggestion
# getShoppingList
# updateShoppingList
# deleteToken
# orderShoppingList

# Create your views here.
@csrf_exempt
def createUser(request):
    if request.method == 'POST':
        
        # Collect Data from request body.
        data = json.loads(request.read())
        
        try:
            newUser = Users(email=data['email'], password=data['password'], name=data['name'])
            newUser.save()
            return HttpResponse(status=204)
        except Exception:
            return HttpResponse(status=500)
        
    elif request.method == 'OPTIONS':
        return HttpResponse(status=204)
    
    return HttpResponse(status=404)
    
@csrf_exempt
def getToken(request):
    if request.method == 'POST':
        # Collect Data from request body.
        data = json.loads(request.read())
        
        search = None
        try:
            search = Users.objects.filter(email=data['email'], password=data['password'])
        except Users.DoesNotExist:
            print("Login Failed")
            return JsonResponse({'error':"Login Failed"})
        
        if search != None:
            token = uuid.uuid4()
            search.update(token = token)
            search.update(expDate = datetime.date.today() + datetime.timedelta(days=1))
            return JsonResponse({'token':token})
            
        
        
    elif request.method == 'OPTIONS':
        return HttpResponse(status=204)
    
    return HttpResponse(status=404)
    
@csrf_exempt
def refreshToken(request):
    if request.method == 'POST':
        # Collect Data from request body.
        data = json.loads(request.read())
        
        search = Users.objects.get(token=data['token'])
        search.token = uuid.uuid4()
        search.expDate = datetime.date.today() + datetime.timedelta(days=1)
        
        return JsonResponse({'token':search.token})
        
    elif request.method == 'OPTIONS':
        return HttpResponse(status=204)
    
    HttpResponse(status=204)
    
@csrf_exempt
def addItem(request):
    # TODO: Rename to 'addItem'
    if request.method == 'POST':
        # Collect Data from request body.
        data = json.loads(request.read())
        
        user = None
        try:
            user = Users.objects.get(token=data['token'])
            if (user.expDate < datetime.date.today()):
                return HttpResponse(status=401)
        except Users.DoesNotExist:
            return HttpResponse(status=401)
        
        
        existFood = None
        try:
            existFood = Foods.objects.get(barcode=data['barcode'])
        except Foods.DoesNotExist:
        
            d = pq(url='https://www.barcodelookup.com/' + data['barcode'])
            itemName = d("#img_preview").attr('alt')
            price = d("html body div.container div.row div.col-md-6.col-md-push-6.online-stores div.store-list ol li a span.store-link").text()
            category = d("#body-container > section.mid-inner > div > div > div.col-md-6.product-details > div:nth-child(5) > div > div:nth-child(2) > div > div > span").text()
        
            existFood = Foods(barcode = data['barcode'], name=itemName, expDate=datetime.date.today(), foodType=category)
            existFood.save()
        
        search = None
        try:
            search = Pantry.objects.get(userID=user.id, foodID=existFood)
            search.count += 1
            search.save()
            
            thingy = {'name':search.foodID.name, 'quantity':search.count, 'barcode':data['barcode']}
            
            return JsonResponse(thingy, safe=False)
            
        except Pantry.DoesNotExist:
            
            category = category.split('>', 1)[-1]
                
            newPantryEntry = Pantry(userID=user, foodID=existFood, count=1)
            existFood = model_to_dict(existFood)
            existFood['quantity'] = 1
            newPantryEntry.save()
            return JsonResponse(existFood, safe=False)
        
    elif request.method == 'OPTIONS':
        return HttpResponse(status=204)
    
    return HttpResponse(status=204)
    
@csrf_exempt
def getItem(request):
    if request.method == 'POST':
        # Collect Data from request body.
        data = json.loads(request.read())
        
        search = None
        try:
            search = Users.objects.get(token=data['token'])
            if (search.expDate < datetime.date.today()):
                return HttpResponse(status=401)
        except Users.DoesNotExist:
            return HttpResponse(status=401)
        
        
        search = None
        try:
            search = Foods.objects.get(barcode=data['barcode'])
            search = model_to_dict(search)
            return JsonResponse(search, safe=False)
        except Foods.DoesNotExist:
            return HttpResponse(status=404)
        
    elif request.method == 'OPTIONS':
        return HttpResponse(status=204)
    
    return HttpResponse(status=204)
    
@csrf_exempt
def getItems(request):
    if request.method == 'POST':
        # Collect Data from request body.
        data = json.loads(request.read())
        
        curUser = None
        try:
            curUser = Users.objects.get(token=data['token'])
            if (curUser.expDate < datetime.date.today()):
                return HttpResponse(status=401)
        except Users.DoesNotExist:
            return HttpResponse(status=401)
        
        
        search = None
        try:
            print(curUser.id)
            search = Pantry.objects.filter(userID=curUser.id).exclude(count=0)
            
            thingy = []
            for item in range(0, len(search)):
                thingy.append({'name': search[item].foodID.name, 'quantity':search[item].count, 'barcode':search[item].foodID.barcode})
            
            print(thingy)
            # search = model_to_dict(search[0])
            return JsonResponse(thingy, safe=False)
        except Pantry.DoesNotExist:
            return HttpResponse(status=404)
        
    elif request.method == 'OPTIONS':
        return HttpResponse(status=204)
    
    return HttpResponse(status=204)
    
@csrf_exempt
def deleteItem(request):
    if request.method == 'POST':
        # Collect Data from request body.
        data = json.loads(request.read())
    
        curUser = None
        try:
            curUser = Users.objects.get(token=data['token'])
            if (curUser.expDate < datetime.date.today()):
                return HttpResponse(status=401)
        except Users.DoesNotExist:
            return HttpResponse(status=401)
        
        search = None
        try:
            searchFood = Foods.objects.filter(barcode=data['barcode'])
            search = Pantry.objects.filter(foodID=searchFood[0].id, userID=curUser.id)
            for st in search:
                if (st.count > 0):
                    st.count -= 1
                    st.save()
                return HttpResponse(status=204)
        except Foods.DoesNotExist:
            return HttpResponse(status=404)
        
        return HttpResponse(status=204)
        
    elif request.method == 'OPTIONS':
        return HttpResponse(status=204)
    
    return HttpResponse(status=204)
    
@csrf_exempt
def getRecipes(request):
    if request.method == 'POST':
        # Collect Data from request body.
        data = json.loads(request.read())
    
        search = None
        try:
            print(data['token'])
            search = Users.objects.get(token=data['token'])
            if (search.expDate < datetime.date.today()):
                return HttpResponse(status=401)
        except Users.DoesNotExist:
            return HttpResponse(status=401)
            
        
        result = Recipes.objects.get(name__contains=data['search'])
        
        result = model_to_dict(result)
        
        return JsonResponse(result, safe=False)
        
    elif request.method == 'OPTIONS':
        return HttpResponse(status=204)
    
    return HttpResponse(status=204)
    
@csrf_exempt
def getRecipe(request):
    if request.method == 'POST':
        # Collect Data from request body.
        data = json.loads(request.read())
    
        search = None
        try:
            search = Users.objects.get(token=data['token'])
            if (search.expDate < datetime.date.today()):
                return HttpResponse(status=401)
        except Users.DoesNotExist:
            return HttpResponse(status=401)
            
        
        result = Recipes.objects.get(name__contains=data['search'])
        
        
        
        return JsonResponse(result, safe=False)
        
    elif request.method == 'OPTIONS':
        return HttpResponse(status=204)
    
    return HttpResponse(status=204)
    
@csrf_exempt
def getSuggestions(request):
    if request.method == 'POST':
        # Collect Data from request body.
        data = json.loads(request.read())
    
        # Check token
        token = data['token']
        
        suggestions = {0 : 'Useful tip #8675309: Try getting a life.', 1:'You\'ve spent 3862.84$ on soda recently...'}
                
        return JsonResponse(suggestions)
        
    elif request.method == 'OPTIONS':
        return HttpResponse(status=204)
    
    return HttpResponse(status=204)
    
@csrf_exempt
def dismissSuggestion(request):
    if request.method == 'POST':
        # Collect Data from request body.
        data = json.loads(request.read())
    
        # Check token
        token = data['token']
        searchID = data['searchID']
        
        # Call Database procedure
        #  EX: token = MONGODB.DISMISSSUGGESTIONS(id, searchID)
        
        return HttpResponse(status=204)
        
    elif request.method == 'OPTIONS':
        return HttpResponse(status=204)
    
    return HttpResponse(status=204)
    
@csrf_exempt
def getShoppingList(request):
    if request.method == 'POST':
        # Collect Data from request body.
        data = json.loads(request.read())
    
        curUser = None
        try:
            curUser = Users.objects.get(token=data['token'])
            if (curUser.expDate < datetime.date.today()):
                return HttpResponse(status=401)
        except Users.DoesNotExist:
            return HttpResponse(status=401)
        
        search = None
        try:
            print(curUser.id)
            search = Pantry.objects.filter(userID=curUser.id, count=0)
            thingy = []
            for item in range(0, len(search)):
                thingy.append({'name': search[item].foodID.name, 'barcode':search[item].foodID.barcode})
            
            print(thingy)
            # search = model_to_dict(search[0])
            return JsonResponse(thingy, safe=False)
        except Pantry.DoesNotExist:
            return HttpResponse(status=404)
        
    elif request.method == 'OPTIONS':
        return HttpResponse(status=204)
    
    return HttpResponse(status=204)
    
@csrf_exempt
def updateShoppingList(request):
    if request.method == 'POST':
        # Collect Data from request body.
        data = json.loads(request.read())
    
        # Check token
        token = data['token']
        search = data['search']
        
        # Call Database procedure
        #  EX: token = MONGODB.GETRECIPES(id, barcodes)
        
        return HttpResponse(status=204)
        
    elif request.method == 'OPTIONS':
        return HttpResponse(status=204)
    
    return HttpResponse(status=204)
    
@csrf_exempt
def deleteToken(request):
    if request.method == 'POST':
        # Collect Data from request body.
        data = json.loads(request.read())
        
        search = None
        try:
            search = Users.objects.get(token=data['token'])
        except Users.DoesNotExist:
            return HttpResponse(status=404)
        
        if search != None:
            search.expDate = datetime.date.today()
            search.save()
            search.update(token=None)
            return HttpResponse(status=204)
        
    elif request.method == 'OPTIONS':
        return HttpResponse(status=204)
    
    return HttpResponse(status=204)
    
@csrf_exempt
def orderShoppingList(request):
    if request.method == 'POST':
        # Collect Data from request body.
        data = json.loads(request.read())
    
        # Check token
        token = data['token']
        
        # Call Database procedure
        #  EX: token = MONGODB.GETRECIPES(id, search)
        
        shoppingOrder = {'OrderID': 'Description'}
        
        return JsonResponse(shoppingOrder)
                
    elif request.method == 'OPTIONS':
        return HttpResponse(status=204)
    
    return HttpResponse(status=204)
