from django.db import models
from django.utils import timezone
import datetime

# api/models.py
class Users(models.Model):
    email = models.EmailField(max_length=200, unique=True)
    name = models.CharField(max_length=200)
    password = models.CharField(max_length=200)
    token = models.UUIDField(editable=False, blank=True)
    expDate = models.DateField(default=timezone.now)
    
class Foods(models.Model):
    barcode = models.CharField(max_length=50, unique=True)
    name = models.CharField(max_length=32000)
    foodType = models.CharField(max_length=32000)
    expDate = models.DateField(default=timezone.now)
    # img = models.ImageField(blank=True)
    
class Recipes(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=32000)
    
class Pantry(models.Model):
    userID = models.ForeignKey(Users, on_delete=models.CASCADE)
    foodID = models.ForeignKey(Foods, on_delete=models.CASCADE)
    count = models.IntegerField(default = 0)
    
class Ingredients(models.Model):
    recipeID = models.ForeignKey(Recipes, on_delete=models.CASCADE)
    foodID = models.ForeignKey(Foods, on_delete=models.CASCADE)

