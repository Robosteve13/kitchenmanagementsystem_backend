from django.urls import path
from . import views


app_name = 'testAPI'  # here for namespacing of urls.

# createUser
# getToken
# refreshToken
# createItem
# getItem
# deleteItem
# getRecipes
# getRecipe
# getSuggestions
# dismissSuggestion
# getShoppingList
# updateShoppingList
# deleteToken
# orderShoppingList

urlpatterns = [
    path("create/user", views.createUser, name="Create User"),
    path("get/token", views.getToken, name="Get Token"),
    path("refresh/token", views.refreshToken, name="Refresh Token"),
    path("add/item", views.addItem, name="Create Item"),
    path("get/item", views.getItem, name="Get Item"),
    path("get/items", views.getItems, name="Get Items"),
    path("delete/item", views.deleteItem, name="Delete Item"),
    path("get/recipes", views.getRecipes, name="Get Recipes"),
    path("get/recipe", views.getRecipe, name="Get Recipe"),
    path("get/suggestions", views.getSuggestions, name="Get Suggestions"),
    path("dismiss/suggestion", views.dismissSuggestion, name="Dismiss Suggestion"),
    path("get/shopping-list", views.getShoppingList, name="Get Shopping List"),
    path("update/shopping-list", views.updateShoppingList, name="Update Shopping List"),
    path("delete/token", views.deleteToken, name="Delete Token"),
    path("order/shopping-list", views.orderShoppingList, name="Order Shopping List"),
]
